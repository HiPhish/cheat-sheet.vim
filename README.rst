.. default-role:: code

##########################################################
 Cheat-Sheet.vim: cheat sheets at the tip of your fingers
##########################################################

What where the components of the CSS box model? How do I map a list in that
weird language I hardly ever use? What are common phrases in a foreign
language? With cheat sheets the answer is right at hand, with cheat-sheet.vim
there is no need to switch to another application.

Why not just use Vim's built-in help instead? There are two main reasons for
making a separate plugin:

- Cheat sheets would be polluting the help namespace; the help is intended for
  Vim manuals, a document about the CSS box model does not belong in there.
- The `help` file type is not always appropriate, when writing a cheat sheet
  for a markup language or a programming language it would make more sense to
  write that cheat sheet in that language as well.


Installation
############

Install it like any other Vim plugin. Cheat sheets are installed separately and
it is up to the author how they want to distribute them.


Quickstart
##########

Use the `:Cheatsheet` command to open a cheat sheet. This plugin comes with one
example cheat sheet included:

.. code-block:: vim

   :Cheatsheet cheat-sheet.txt

You do not have to spell out the entire name:

.. code-block:: vim

   :Cheatsheet cheat

If there are multiple matches you will be offered a list of suggestions.


License
#######

Cheat-sheet.vim is licensed under the terms of the MIT license. Please see the
`COPYING.rst`_ file for details.

.. _COPYING.rst: COPYING.rst
