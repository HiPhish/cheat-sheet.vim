" Author: Alejandro "HiPhish" Sanchez
" License:  The MIT License (MIT) {{{
"    Copyright (c) 2017 HiPhish
" 
"    Permission is hereby granted, free of charge, to any person obtaining a
"    copy of this software and associated documentation files (the
"    "Software"), to deal in the Software without restriction, including
"    without limitation the rights to use, copy, modify, merge, publish,
"    distribute, sublicense, and/or sell copies of the Software, and to permit
"    persons to whom the Software is furnished to do so, subject to the
"    following conditions:
" 
"    The above copyright notice and this permission notice shall be included
"    in all copies or substantial portions of the Software.
" 
"    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
"    OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
"    MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
"    NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
"    DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
"    OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
"    USE OR OTHER DEALINGS IN THE SOFTWARE.
" }}}
" vim: set noexpandtab softtabstop=0 shiftwidth=4 tabstop=4

if !has('nvim') || exists('g:cheat_sheet')
  finish
endif
let g:cheat_sheet = 1


command! -nargs=* Cheatsheet call <SID>cheatsheet(<q-mods>, <q-args>)

function! s:cheatsheet(mods, sheet)
	" Here is how we find out cheat sheet: first get the list of all runtime
	" path directories.
	let l:files = split(&rtp, ',')
	" Now map each of them to the path to its 'cheat-sheets' directory
	let l:files = map(l:files, 'globpath(v:val, "cheat-sheets/")')
	" Not every path has such a directory, so filter out those ones
	let l:files = filter(l:files, '!empty(v:val)')
	" Replace each cheat sheet directory with its contents
	let l:files = map(l:files, 'globpath(v:val, "*")')
	let l:files = map(l:files, 'split(v:val, "\n")')
	let l:files = s:flatten(l:files)
	" Finally filter out the files that do not match the sheet name
	let l:files = filter(l:files, 'match(v:val, "\\v'.a:sheet.'[^/]*$") >= 0')

	if empty(l:files)
		echohl ErrorMsg
		echo 'No cheat sheet named '''.a:sheet.''' defined.'
		echohl None
		return
	endif

	if len(l:files) == 1
		let l:file = l:files[0]
	else
		let l:suggestions = copy(l:files)
		for l:i in range(len(l:suggestions))
			let l:suggestions[i] = (l:i+1).'. '.l:suggestions[l:i]
		endfor
		call insert(l:suggestions, 'Please chose your cheat sheet')
		let l:file = l:files[inputlist(l:suggestions) - 1]
	endif

	" If there is a window open with the cheat sheet focus on it
	if bufwinnr(l:file) > 0
		silent execute bufwinnr(l:file) 'wincmd w'
		return
	endif

	execute a:mods 'new'
	execute 'edit' l:file
	set nomodifiable
endfunction

function! s:flatten(items)
	let l:result = []
	for l:item in a:items
		let l:result += l:item
	endfor
	return l:result
endfunction
